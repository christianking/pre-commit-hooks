# pre-commit-hooks



## Using pre-commit-hooks

If you've never used pre-commit hooks before, you can find out more about them [here](https://pre-commit.com/)

## Available pre-commit hooks

You can find all of the available hooks in .pre-commit-hooks.yaml. Comments in this file describe the purpose of each hook.
