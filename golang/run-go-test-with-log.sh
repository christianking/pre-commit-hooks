#!/usr/bin/env bash

fail() {
  echo "unit tests failed"
  exit 1
}

LOG="./go-test.log"
FILES=$(go list ./... | grep -v /vendor/) || fail
go test -v ${FILES} > $LOG
RET=$?
cat $LOG | grep -v -E '^[[:digit:]]{4}/[[:digit:]]{2}/[[:digit:]]{2}'
exit $RET
