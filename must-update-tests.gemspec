# frozen_string_literal: true

Gem::Specification.new do |s|
  s.name        = 'must-update-tests'
  s.version     = '0.0.3'
  s.summary     = 'pre-commit hook for checking tests are updated'
  s.description =
    'A ruby gem for installation with pre-commit which forces you to update tests when updating the code they test'
  s.authors     = ['Christian King']
  s.email       = 'christianking@gitlab.com'
  s.files       = ['bin/must-update-tests']
  s.homepage    =
    'https://gitlab.com/christianking/pre-commit-hooks'
  s.executables << 'must-update-tests'
  s.required_ruby_version = '>= 2.7.0'
  s.add_dependency 'optparse', '~> 0.4.0'
end
