HOOKS_DIR := $(shell pwd)

tmpdir:
	$(eval TMP := $(shell mktemp -d))
	git clone https://gitlab.com/christianking/pre-commit-hooks-testdata.git $(TMP)
	cd $(TMP); /bin/bash prepare-test-environment.sh

rmtmp:
	rm -rf $(TMP)

define expect_success
	cd $(TMP); pre-commit try-repo $(HOOKS_DIR) $(1) --files $(2)
endef

define expect_failure
		cd $(TMP); ! pre-commit try-repo $(HOOKS_DIR) $(1) --files $(2); RES=$$?; [ $$RES -ne 0 ] && echo "Got unexpected success code $$RES!" || echo "Got expected failure."; ! exit $$RES
endef

test-golangci-basic:
	$(call expect_success,golangci-lint,main.go)

test-gofmt-basic:
	$(call expect_success,go-fmt,main.go)

test-markdownlint-cli2-pass:
	$(call expect_success,markdownlint-cli2,good.md)

test-markdownlint-cli2-fail:
	$(call expect_failure,markdownlint-cli2,bad.md)

test-must-update-tests-pass:
	$(call expect_success,must-update-tests-golang,main.go main_test.go)

test-must-update-tests-fail:
	$(call expect_failure,must-update-tests-golang,main.go)

test-must-update-tests-newer-test-file:
	$(call expect_success,must-update-tests-golang,newer_test_file.go)

test: tmpdir \
	test-golangci-basic \
	test-gofmt-basic \
	test-markdownlint-cli2-pass \
	test-markdownlint-cli2-fail \
	test-must-update-tests-pass \
	test-must-update-tests-fail \
	test-must-update-tests-newer-test-file \
	rmtmp
